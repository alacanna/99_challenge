# :warning: Importante :warning:

Na 99 é muito comum termos libs escritas em **Objc** e/ou **Swift**. Nesse caso gostaríamos de saber como você lida com esse tipo de complexidade.

Você vai encontrar nesse repositório dois projetos. o `Personas` e o `PersonasSDK`

Esperamos que você faça toda a camada de modelagem e API via SDK em **ObjC** e use esses dados para desenhar a tela no projeto principal em **Swift**

# Desafio mobile

Tivemos um problema durante o desenvolvimento do projeto, pois encontramos um bug na nossa API que teremos que resolver no lado do aplicativo.

A listagem de pessoas estava retornando dados duplicados, o que não deveria acontecer pois eles possuem um identificador único.

Como podemos evitar a exibição repetida? 

:warning: Não exiba os dados duplicados na tabela, mostrando apenas uma das informações. Na 99 damos muito valor a qualidade de código.



## Instruções

- Faça um fork desse projeto;
- Dentro da pasta `api` utilize o seguinte comando: `java -jar stubby4j-5.0.1.jar -d 99.yml`
- Você terá acesso a resposta da api através da url: `http://localhost:8882/personas/`
- Modele uma classe que represente o `personas`, as datas estão formatadas em `ISO8601`;
- Carregue esses dados e exiba na tela, deixamos com você o layout da interface;
- Ao finalizar, comunique ao recrutador para que possamos analisar seu teste;
- Não envie um pull request para esse repositório. Já teremos acesso ao seu fork e faremos a avaliaço a partir dele.

## Dicas

- Não é permitido alterar o json;
- Você pode utilizar quaisquer biblioteca que quiser;
- Damos muito valor na interface visual;
- Testes (unitários, integração, aceitação etc.) são essenciais.
- Recomendamos você usar ViewCode para desenhar as telas :)

## Dúvidas

Paulo Mendes - paulo.mendes@99taxis.com

Boa sorte! :beers::beers:
