package com.amandalacanna.api.manager


class Constants {
    object ResponseCode {
        val OK = 200
    }

    object ResponseFile {
        val PERSONAS = "personas.json"
    }
}
