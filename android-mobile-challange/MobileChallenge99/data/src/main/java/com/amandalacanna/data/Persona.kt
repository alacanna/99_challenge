package com.amandalacanna.data

import java.io.Serializable

data class Persona(val id: String,
                   val name: String,
                   val image: String,
                   val birthday: String,
                   val bio: String?): Serializable