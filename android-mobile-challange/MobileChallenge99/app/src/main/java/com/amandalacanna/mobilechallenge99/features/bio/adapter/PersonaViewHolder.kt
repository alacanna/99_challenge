package com.amandalacanna.mobilechallenge99.features.bio.adapter

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.support.v7.widget.RecyclerView
import android.view.View
import com.amandalacanna.data.Persona
import com.amandalacanna.data.extension.toDate
import com.amandalacanna.mobilechallenge99.databinding.PagerItemBinding
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target


class PersonaViewHolder (private val binding: PagerItemBinding) : RecyclerView.ViewHolder(binding.root) {
    fun setDate(persona: Persona) {
        binding.txtBirth.text = persona.birthday.toDate().toString()
    }

    fun setThumbnail (persona: Persona){

        Picasso.with(binding.root.context)
                .load(persona.image)
                .into(object : Target{
                    override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
                        binding.imgPersona.visibility = View.GONE
                     //   binding.loadingItem.visibility = View.VISIBLE
                    }

                    override fun onBitmapFailed(errorDrawable: Drawable?) {
                       // binding.imgThumb.visibility = View.VISIBLE
                       // binding.imgThumb.setImageResource(R.mipmap.ic_launcher)
                       // binding.loadingItem.visibility = View.GONE

                    }

                    override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                      //  binding.loadingItem.visibility = View.GONE
                       // binding.imgThumb.visibility = View.VISIBLE
                        //binding.imgThumb.setImageBitmap(bitmap)

                    }
                })
    }
}