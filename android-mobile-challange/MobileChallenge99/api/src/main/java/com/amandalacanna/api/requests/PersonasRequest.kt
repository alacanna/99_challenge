package com.amandalacanna.api.requests

import com.amandalacanna.data.Persona
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Headers


interface PersonasRequest {
    @Headers("Content-Type: application/json")
    @GET("personas/")
    fun getPersonas(): Observable<List<Persona>>
}
