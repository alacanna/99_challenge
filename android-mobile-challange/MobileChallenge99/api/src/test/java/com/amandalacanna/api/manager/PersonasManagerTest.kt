package com.amandalacanna.api.manager

import com.amandalacanna.api.RestAPI
import com.amandalacanna.api.requests.PersonasRequest
import com.amandalacanna.data.Persona
import org.junit.Before
import org.junit.Test

import io.reactivex.observers.TestObserver
import okhttp3.HttpUrl

import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import com.amandalacanna.api.manager.Constants.ResponseFile.PERSONAS
import com.amandalacanna.api.manager.Constants.ResponseCode.OK
import junit.framework.Assert.*

class PersonasManagerTest {

    private var server: MockWebServer? = null
    private var testSub: TestObserver<List<Persona>>? = null
    private var serverUrl: HttpUrl? = null

    private var personaRequest: PersonasRequest? = null
    private var personaManager: PersonasManager? = null

    @Before
    @Throws(Exception::class)
    fun setup() {
        server = MockWebServer()
        server?.let {
            it.start()
            serverUrl = it.url("")
        }

        val restApi = RestAPI(HttpUrl.parse("http://localhost:8882/"))
        personaRequest = restApi.personas

        personaRequest?.let {
            personaManager = PersonasManager(it)
        }

        testSub = TestObserver()
    }

    @Test
    @Throws(Exception::class)
    fun testAllSetAsNotNull() {
        assertNotNull(server)
        assertNotNull(testSub)
        assertNotNull(serverUrl)
        assertNotNull(personaRequest)
        assertNotNull(personaManager)
    }

    @Test
    @Throws(Exception::class)
    fun testPersonasRequest() {
        val mockResponse = MockResponse()
        mockResponse.setResponseCode(OK)
        mockResponse.setBody(JsonLoader().loadJson(PERSONAS))
        server?.enqueue(mockResponse)

        testSub?.let {
            personaManager?.getPersonas()?.subscribe(it)

            // Should have no error
            it.assertNoErrors()
            it.assertNoTimeout()
            it.assertSubscribed()

            // Assert Data
            it.assertOf { resultTestObserver ->
                assertResultsDistinct(resultTestObserver)
            }
        }
    }

    private fun assertResultsDistinct(resultTestObserver: TestObserver<List<Persona>>) {

        // Expected Data

        val totalResults = 6

        val id_1 = "97d16abc-1569-43e0-929b-cef05cd850fb"
        val name_1 =  "Steve Jobs"
        val image_1 = "http://adsoftheworld.com/files/steve-jobs.jpg"
        val birthday_1 = "1955-02-24T00:00:00Z"
        val bio_1 = "Steven Paul Jobs was an American businessman, inventor, and industrial designer. He was the co-founder, chairman,and chief executive officer (CEO) of Apple Inc."

        val id_2 = "599df0e4-183c-4bfb-91c5-881d4550cd3f"
        val name_2 =  "Craig Federighi"
        val image_2 = "http://www.apple.com/pr/bios/images/federighi_hero20120727.png"
        val birthday_2 = "1969-05-27T00:00:00Z"
        val bio_2 =  "Craig Federighi is Apple's senior vice president of Software Engineering. Federighi oversees the development of iOS, macOS and Apple's common operating system engineering teams."

        val id_3 = "145854f5-63a7-456a-9532-9f3a2fcf9875"
        val name_3 =  "Jonathan Ive"
        val image_3 = "http://d.fastcompany.net/multisite_files/codesign/imagecache/1280/poster/2012/10/1671131-poster-1280-jony-ive.jpg"
        val birthday_3 = "1967-02-27T00:00:00Z"
        val bio_3 =  "Sir Jonathan Paul Ive, KBE, is a British industrial designer who is currently the Chief Design Officer of Apple Inc."

        val id_4 = "fe71a57c-fb1f-4f85-82df-3d6261b88a04"
        val name_4 =  "Timothy Cook"
        val image_4 = "http://images.apple.com/pr/bios/images/cook_hero.png"
        val birthday_4 = "1960-11-01T00:00:00Z"


        val id_5 = "9e98ff85-a11f-43de-ab87-f31f5bf0989c"
        val name_5 =  "William Henry Gates"
        val image_5 = "https://pbs.twimg.com/profile_images/5581099545616793600/j1f9DiJi.jpeg"
        val birthday_5 = "1955-10-28T00:00:00Z"
        val bio_5 =  "William Henry Gates III is an American business magnate, investor, author, and philanthropist. In 1975, Gates and Paul Allen co-founded Microsoft, which became the world's largest PC software company."


        val id_6 = "94fb5c91-7fe4-449f-93de-6b493d2d981f"
        val name_6 =  "Scott Forstall"
        val image_6 = "https://gigaom.com/wp-content/uploads/sites/1/2012/10/apple-exec-scott-forstall.jpg?w=237"
        val birthday_6 = "1969-01-01T00:00:00Z"


        // Result from mock
        val personas = resultTestObserver.values()[0]

        // Asserts
        assertEquals(totalResults, personas?.size)

        assertEquals(id_1, personas[0].id)
        assertEquals(name_1, personas[0].name)
        assertEquals(image_1, personas[0].image)
        assertEquals(birthday_1, personas[0].birthday)
        assertEquals(bio_1, personas[0].bio)

        assertEquals(id_2, personas[1].id)
        assertEquals(name_2, personas[1].name)
        assertEquals(image_2, personas[1].image)
        assertEquals(birthday_2, personas[1].birthday)
        assertEquals(bio_2, personas[1].bio)

        assertEquals(id_3, personas[2].id)
        assertEquals(name_3, personas[2].name)
        assertEquals(image_3, personas[2].image)
        assertEquals(birthday_3, personas[2].birthday)
        assertEquals(bio_3, personas[2].bio)


        assertEquals(id_4, personas[3].id)
        assertEquals(name_4, personas[3].name)
        assertEquals(image_4, personas[3].image)
        assertEquals(birthday_4, personas[3].birthday)

        assertEquals(id_5, personas[4].id)
        assertEquals(name_5, personas[4].name)
        assertEquals(image_5, personas[4].image)
        assertEquals(birthday_5, personas[4].birthday)
        assertEquals(bio_5, personas[4].bio)

        assertEquals(id_6, personas[5].id)
        assertEquals(name_6, personas[5].name)
        assertEquals(image_6, personas[5].image)
        assertEquals(birthday_6, personas[5].birthday)

    }
}