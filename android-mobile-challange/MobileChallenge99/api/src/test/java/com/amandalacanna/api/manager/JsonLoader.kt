package com.amandalacanna.api.manager

import com.google.common.io.ByteStreams
import java.io.IOException

class JsonLoader {
    @Throws(IOException::class)
    fun loadJson(filename: String): String {
        val inputStream = this.javaClass.classLoader.getResourceAsStream(filename)
        return String(ByteStreams.toByteArray(inputStream))
    }
}