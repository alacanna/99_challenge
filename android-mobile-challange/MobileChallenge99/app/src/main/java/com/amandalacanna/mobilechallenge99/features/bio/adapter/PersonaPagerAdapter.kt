package com.amandalacanna.mobilechallenge99.features.bio.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.amandalacanna.data.Persona
import com.amandalacanna.mobilechallenge99.features.bio.fragments.PersonaFragment
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.ArrayList


class PersonaPagerAdapter (fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager){

    private var personas: MutableList<Persona> = ArrayList()

    fun setObservablePersonaList(observablePersonaList: Observable<List<Persona>>) {
        observablePersonaList
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ personas ->
                    this@PersonaPagerAdapter.personas.addAll(personas)
                    this@PersonaPagerAdapter.notifyDataSetChanged()
                }, { _ ->

                    this@PersonaPagerAdapter.notifyDataSetChanged()
                })
    }

    override fun getItem(position: Int): Fragment {
        return PersonaFragment.newInstance(personas[position])
    }

    override fun getCount(): Int {
        return personas.size
    }
}