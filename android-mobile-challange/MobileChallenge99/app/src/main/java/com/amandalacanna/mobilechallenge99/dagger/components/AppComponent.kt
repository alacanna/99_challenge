package com.amandalacanna.mobilechallenge99.dagger.components

import android.app.Application
import com.amandalacanna.mobilechallenge99.application.AppChallenge
import com.amandalacanna.mobilechallenge99.dagger.modules.ActivityModule
import com.amandalacanna.mobilechallenge99.dagger.modules.ApiModule
import com.amandalacanna.mobilechallenge99.dagger.modules.AppModule
import com.amandalacanna.mobilechallenge99.dagger.modules.FragmentModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton


@Singleton
@Component(modules = arrayOf(AndroidInjectionModule::class, AppModule::class, ActivityModule::class, FragmentModule::class, ApiModule::class))
interface AppComponent {

    fun inject(application: AppChallenge)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun appModule(appModule: AppModule): Builder
        fun build(): AppComponent
    }
}