package com.amandalacanna.mobilechallenge99.dagger.modules

import com.amandalacanna.mobilechallenge99.features.bio.activities.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
internal abstract class ActivityModule {
    @ContributesAndroidInjector(modules = arrayOf(FragmentModule::class))
    abstract fun contribuiteSplashActivity(): MainActivity

}