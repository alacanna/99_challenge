package com.amandalacanna.mobilechallenge99.features.bio.viewModel

import com.amandalacanna.api.manager.PersonasManager
import com.amandalacanna.data.Persona
import dagger.Reusable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject


interface PersonaViewModelInputs{
    fun loadBios()
}

interface PersonaViewModelOutputs{
    val personas: Observable<List<Persona>>
    val isLoading: Observable<Boolean>
    val errorMessage: Observable<String>

}

interface PersonaViewModelType{
    val inputs: PersonaViewModelInputs
    val outputs: PersonaViewModelOutputs
}

@Reusable
class PersonaViewModel @Inject constructor(): PersonaViewModelInputs, PersonaViewModelOutputs, PersonaViewModelType{

    @Inject
    lateinit var personasManager: PersonasManager

    override val inputs: PersonaViewModelInputs
        get() = this
    override val outputs: PersonaViewModelOutputs
        get() = this

    val personasSubject = PublishSubject.create<List<Persona>>()
    override val personas: Observable<List<Persona>>
        get() = personasSubject

    val isLoadingSubject = PublishSubject.create<Boolean>()
    override val isLoading: Observable<Boolean>
        get() = isLoadingSubject

    val errorMessageSubject = PublishSubject.create<String>()
    override val errorMessage: Observable<String>
        get() = errorMessageSubject

    override fun loadBios() {
        isLoadingSubject.onNext(true)
        personasManager
                .getPersonas()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe ({
                    isLoadingSubject.onNext(false)
                    personasSubject.onNext(it)
                },{
                    error ->
                    isLoadingSubject.onNext(false)
                    errorMessageSubject.onNext("Ops, ocorreu um erro de conexão com o servidor, tente novamente.")
                    error.printStackTrace()
                })
    }

}