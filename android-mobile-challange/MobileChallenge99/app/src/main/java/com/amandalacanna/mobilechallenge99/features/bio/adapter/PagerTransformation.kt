package com.amandalacanna.mobilechallenge99.features.bio.adapter

import android.support.v4.view.ViewPager
import android.view.View


class PagerTransformation(private val viewPager: ViewPager) : ViewPager.PageTransformer{

    override fun transformPage(page: View?, position: Float) {
            val pageWidth = viewPager.measuredWidth.minus(viewPager.paddingLeft).minus(viewPager.paddingRight)
            val paddingLeft = viewPager.paddingLeft
            val transformPos = (page?.left?.minus((viewPager.scrollX.plus(paddingLeft))))?.div(pageWidth)

            transformPos?.let {
                when {
                    transformPos < -1 -> {
                        // This page is way off-screen to the left.
                        page.alpha = 1f
                        page.scaleY = 0.7f
                    }
                    transformPos <= 1 -> // [-1,1]
                        page.scaleY = 1f
                    else -> {
                        // This page is way off-screen to the right.
                        page.alpha = 1f
                        page.scaleY = 0.7f
                    }
                }
            }
    }

}