package com.amandalacanna.mobilechallenge99.features.bio.fragments

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amandalacanna.data.Persona
import com.amandalacanna.data.extension.toDate
import com.amandalacanna.data.extension.format

import com.amandalacanna.mobilechallenge99.R
import com.amandalacanna.mobilechallenge99.dagger.Injectable
import com.amandalacanna.mobilechallenge99.databinding.PagerItemBinding
import com.squareup.picasso.Picasso


class PersonaFragment : Fragment(), Injectable{

    companion object {
        private val PERSONA = "persona"

        fun newInstance(persona: Persona): Fragment {
            val args = Bundle()
            args.putSerializable(PERSONA, persona)

            val fragment = PersonaFragment()
            fragment.arguments = args

            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = DataBindingUtil.inflate<PagerItemBinding>(inflater, R.layout.pager_item, container, false)

        (arguments.getSerializable(PERSONA) as? Persona)?.let {
            binding.persona = it
            binding.txtBirth.text = it.birthday.toDate().format()
            binding.txtBio.text = it.bio ?: "Not Available."
            Picasso.with(binding.root.context)
                    .load(it.image)
                    .error(R.drawable.ic_launcher_foreground)
                    .into(binding.imgPersona)

        }

        return binding.root
    }

}