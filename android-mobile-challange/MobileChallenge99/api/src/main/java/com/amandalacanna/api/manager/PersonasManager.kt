package com.amandalacanna.api.manager

import com.amandalacanna.api.requests.PersonasRequest
import com.amandalacanna.data.Persona
import io.reactivex.Observable
import javax.inject.Inject


class PersonasManager @Inject constructor(val personasRequest: PersonasRequest) {

    fun getPersonas(): Observable<List<Persona>> {
        return personasRequest
                .getPersonas()
                .map { it.distinctBy { (id) -> id } }
    }
}