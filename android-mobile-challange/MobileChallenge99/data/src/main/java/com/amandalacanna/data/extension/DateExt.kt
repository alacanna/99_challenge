package com.amandalacanna.data.extension

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


@Throws(ParseException::class)
fun Date.format(): String {
    val dateFormatter = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
    return dateFormatter.format(this)
}