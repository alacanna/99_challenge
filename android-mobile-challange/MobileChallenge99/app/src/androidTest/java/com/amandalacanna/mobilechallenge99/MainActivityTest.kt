package com.amandalacanna.mobilechallenge99

import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.amandalacanna.mobilechallenge99.features.bio.activities.MainActivity
import org.junit.Rule
import org.junit.runner.RunWith
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.matcher.ViewMatchers.withText
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.espresso.assertion.ViewAssertions.matches

import org.junit.Test
import io.reactivex.plugins.RxJavaPlugins
import org.hamcrest.core.AllOf

@RunWith (AndroidJUnit4::class)
class MainActivityTest {
    @Rule
    @JvmField
    val activityRule = ActivityTestRule<MainActivity>(MainActivity::class.java)


    @Test
    fun appHasExpectedTitleAndControls() {
        onView(withId(R.id.action_refresh)).check(matches(isDisplayed()))
    }

    @Test
    fun appShowsLoadingIndicatorsWhileLoading() {
        onView(withId(R.id.action_refresh)).perform(click())
        if (RxJavaPlugins.isLockdown()) {
            run {
                onView(withId(R.id.progressBar)).check(matches(isDisplayed()))
            }
        }
    }

    @Test
    fun testSwipeBetweenFirstAndSecondPage() {
        onView(withText("Steve Jobs")).check(matches(isDisplayed()))
        onView(withId(R.id.viewPager)).perform(swipeLeft())
        onView(withText("Craig Federighi")).check(matches(isDisplayed()))
    }

    @Test
    fun testSwipeBetweenFirstSecondAndBackToFirstPage() {
        onView(AllOf.allOf(withId(R.id.txt_name), withText("Steve Jobs"))).check(matches(isDisplayed()))
        onView(withId(R.id.viewPager)).perform(swipeLeft())
        onView(withText("Craig Federighi")).check(matches(isDisplayed()))
        onView(withId(R.id.viewPager)).perform(swipeRight())
        onView(withText("Steve Jobs")).check(matches(isDisplayed()))
    }

    @Test
    fun testSwipeToTheEnd() {
        onView(withText("Steve Jobs")).check(matches(isDisplayed()))
        onView(withId(R.id.viewPager))
                .perform(swipeLeft())
                .perform(swipeLeft())
                .perform(swipeLeft())
                .perform(swipeLeft())
                .perform(swipeLeft())
                .perform(swipeLeft())
        onView(withText("Scott Forstall")).check(matches(isDisplayed()))
    }

}
