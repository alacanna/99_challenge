package com.amandalacanna.mobilechallenge99.dagger.modules

import com.amandalacanna.mobilechallenge99.features.bio.fragments.PersonaFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
internal abstract class FragmentModule{
    @ContributesAndroidInjector
    internal abstract fun contribuiteMenuFragment(): PersonaFragment
}
