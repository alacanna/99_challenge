package com.amandalacanna.data.extension

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


@Throws(ParseException::class)
fun String.toDate(dateFormat: String? = "yyyy-MM-dd'T'HH:mm:ss'Z'"): Date {
    val dateFormatter = SimpleDateFormat(dateFormat, Locale.getDefault())
    return dateFormatter.parse(this)
}
