package com.amandalacanna.mobilechallenge99.dagger.modules

import com.amandalacanna.api.RestAPI
import com.amandalacanna.api.manager.PersonasManager
import com.amandalacanna.api.requests.PersonasRequest
import com.amandalacanna.data.Persona
import dagger.Module
import dagger.Provides
import io.reactivex.Observable
import javax.inject.Singleton

@Module
class ApiModule {
    @Provides
    @Singleton
    internal fun provideRestAPI(): RestAPI {
        return RestAPI()
    }

    @Provides
    @Singleton
    internal fun providePersonaRequest(restAPI: RestAPI): PersonasRequest{
        return restAPI.personas
    }

}