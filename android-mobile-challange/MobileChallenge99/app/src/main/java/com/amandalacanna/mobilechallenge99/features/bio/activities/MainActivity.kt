package com.amandalacanna.mobilechallenge99.features.bio.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.amandalacanna.mobilechallenge99.R
import com.amandalacanna.mobilechallenge99.features.bio.adapter.PersonaPagerAdapter
import com.amandalacanna.mobilechallenge99.features.bio.adapter.PagerTransformation
import com.amandalacanna.mobilechallenge99.features.bio.viewModel.PersonaViewModel
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


class MainActivity : AppCompatActivity(), HasSupportFragmentInjector {
    private val disposables: CompositeDisposable by lazy { CompositeDisposable() }

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var viewModel: PersonaViewModel

    private val personaAdapter: PersonaPagerAdapter by lazy { PersonaPagerAdapter(supportFragmentManager) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bindOutputs()
        viewModel.inputs.loadBios()
        setUpViewPager()
    }

    // Bind view model outputs
    fun bindOutputs() = with(viewModel.outputs) {

        val isLoadingDisposable = isLoading.subscribe {
            isLoading ->
            if (isLoading)
            {
                pager_container.visibility = View.GONE
                progressBar.visibility = View.VISIBLE
            } else {
                pager_container.visibility = View.VISIBLE
                progressBar.visibility = View.GONE
            }
        }

        val errorMessageDisposable = errorMessage.subscribe {
            Toast.makeText(this@MainActivity, it, Toast.LENGTH_LONG).show()
        }

        disposables.addAll(isLoadingDisposable, errorMessageDisposable)
    }

    private fun setUpViewPager() {
        viewPager.pageMargin = 20
        personaAdapter.setObservablePersonaList(viewModel.outputs.personas)
        viewPager.adapter = personaAdapter
        viewPager.setPageTransformer(false, PagerTransformation(viewPager))
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return dispatchingAndroidInjector
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_refresh -> {
                viewModel.inputs.loadBios()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

}

